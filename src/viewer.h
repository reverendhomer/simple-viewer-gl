/**********************************************\
*
*  Simple Viewer GL edition
*  by Andrey A. Ugolnik
*  http://www.ugolnik.info
*  andrey@ugolnik.info
*
\**********************************************/

#pragma once

#include "common/callbacks.h"
#include "common/scale.h"
#include "types/vector.h"

#include <vector>
#include <memory>

class cCheckerboard;
class cDeletionMark;
class cExifPopup;
class cFilesList;
class cImageBorder;
class cImageLoader;
class cInfoBar;
class cPixelPopup;
class cProgress;
class cQuadImage;
class cSelection;
struct GLFWwindow;
struct sConfig;

class cViewer final : public iCallbacks
{
public:
    explicit cViewer(sConfig& config);
    ~cViewer();

    void setWindow(GLFWwindow* window);

    void addPaths(const char** paths, int count);

    void render();
    void update();
    bool isUploading() const;

    bool isWindowModeRequested() const
    {
        return m_windowModeChangeRequested;
    }

    bool isWindowed() const
    {
        return m_isWindowed;
    }
    void setWindowed(bool windowed)
    {
        m_isWindowed = windowed;
    }

    const Vectori& getWindowPosition() const
    {
        return m_prevPos;
    }
    const Vectori& getWindowSize() const
    {
        return m_prevSize;
    }

public:
    virtual void startLoading() override;
    virtual void doProgress(float progress) override;
    virtual void endLoading() override;

    void fnResize(const Vectori& size);
    void centerWindow();
    void fnPosition(const Vectori& pos);
    void fnMouse(const Vectorf& pos);
    void fnCursorEnter(bool entered);
    void fnMouseScroll(const Vectorf& pos);
    void fnMouseButtons(int button, int action, int mods);
    void fnKeyboard(int key, int scancode, int action, int mods);
    void showCursor(bool show);
    void loadImage(int step);

private:
    void loadSubImage(int subStep);
    void calculateScale();
    void updateScale(bool up);
    void updateFiltering();
    void updateInfobar();
    void updatePixelInfo(const Vectorf& pos);

    void keyUp();
    void keyDown();
    void keyLeft();
    void keyRight();
    void shiftCamera(const Vectorf& delta);
    Vectorf screenToImage(const Vectorf& pos) const;
    Vectorf calculateMousePosition(const Vectorf& pos) const;
    void updateMousePosition();
    void enablePixelInfo(bool show);

private:
    sConfig& m_config;
    Vectorf m_ratio;
    bool m_imagePrepared = false;
    cScale m_scale;
    bool m_isWindowed;
    bool m_cursorInside = false;
    bool m_windowModeChangeRequested = false;
    bool m_mouseLB, m_mouseMB, m_mouseRB;
    Vectorf m_lastMouse;
    Vectorf m_camera;
    Vectori m_prevPos;
    Vectori m_prevSize;
    int m_angle;

    bool m_subImageForced = false;
    bool m_animation = false;
    float m_animationTime = 0.0f;

    std::unique_ptr<cQuadImage> m_image;
    std::unique_ptr<cFilesList> m_filesList;
    std::unique_ptr<cProgress> m_progress;
    std::unique_ptr<cImageLoader> m_loader;
    std::unique_ptr<cInfoBar> m_infoBar;
    std::unique_ptr<cPixelPopup> m_pixelPopup;
    std::unique_ptr<cExifPopup> m_exifPopup;
    std::unique_ptr<cCheckerboard> m_checkerBoard;
    std::unique_ptr<cDeletionMark> m_deletionMark;
    std::unique_ptr<cImageBorder> m_border;
    std::unique_ptr<cSelection> m_selection;
};
